let barChart = null;

function getMonthlyData() {
    const months = [
        'january', 'february', 'march', 'april', 'may', 'june',
        'july', 'august', 'september', 'october', 'november', 'december'
    ];
    const incomeData = [];
    const expensesData = [];

    months.forEach(month => {
        const income = document.getElementById(`income-${month}`).value || 0;
        const expenses = document.getElementById(`expenses-${month}`).value || 0;
        incomeData.push(parseFloat(income));
        expensesData.push(parseFloat(expenses));
    });

    return { incomeData, expensesData };
}

function initializeChart() {
    if (barChart) {
        barChart.destroy();
    }
    
    var ctx = document.getElementById('barChart').getContext('2d');
    var { incomeData, expensesData } = getMonthlyData();
    barChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            datasets: [{
                label: 'Income',
                data: incomeData,
                backgroundColor: 'rgba(75, 192, 192, 0.2)',
                borderColor: 'rgba(75, 192, 192, 1)',
                borderWidth: 1
            }, {
                label: 'Expenses',
                data: expensesData,
                backgroundColor: 'rgba(255, 99, 132, 0.2)',
                borderColor: 'rgba(255, 99, 132, 1)',
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
}

function setupChartTab() {
    console.log('Setting up chart tab...');
    
    // Use Bootstrap's tab event
    document.getElementById('chart-tab').addEventListener('shown.bs.tab', function (e) {
        console.log('Chart tab shown');
        setTimeout(initializeChart, 0);
    });
}

function downloadChart() {
    const canvas = document.getElementById('barChart');
    // Create a temporary link element
    const link = document.createElement('a');
    link.download = 'income-expenses-chart.png';
    link.href = canvas.toDataURL('image/png');
    // Trigger the download
    link.click();
}

// Update the DOMContentLoaded listener
document.addEventListener('DOMContentLoaded', function() {
    console.log('DOM loaded, setting up chart...');
    setupChartTab();
    
    // Add download button handler
    document.getElementById('download').addEventListener('click', downloadChart);
});