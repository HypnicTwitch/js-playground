# js-playground

## Purpose
Repository for experiments and training with javascript

## Contents
```
Js-playground
├── [JSBasics4Beginners](https://accesso.udemy.com/course/javascript-basics-for-beginners/learn/lecture/10688890#overview_)
│   └── Udemy crash course on modern javascript. Taking it to refresh basics and learn more advanced OOP features
├── [Bucks2Bars](https://accesso.udemy.com/course/github-copilot/)
│   └── Udemy course, GitHub Copilot Beginner to Pro
└── README.md
```