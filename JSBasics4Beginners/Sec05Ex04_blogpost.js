/** create a "blog post" object literal with the following characteristics
 * Title
 * body 
 * author
 * number of views
 * comments section
 *   comment-author
 *   comment-body
 * isLive
 */
let post = {
    title: 'blog title',
    body: 'body text',
    author: 'blog author',
    'view-count': 0,
    'comments': [
        {author: 'author 1', body: 'body 01'},
        {author: 'author 2', body: 'body 02'},
    ],
    isLive: false
}

console.log(post)