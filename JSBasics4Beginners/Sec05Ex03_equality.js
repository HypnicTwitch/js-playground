
let address1 = new Address('a', 'b', 'c')
let address2 = new Address('a', 'b', 'c')

//Constructor function
function Address(theStreet, theCity, theZip) {
    this.street = theStreet
    this.city = theCity
    this.zipCode = theZip
}

//deep compare
function areEqual(address1, address2) {
    if(address1.street === address2.street
        && address1.city === address2.city
        && address1.zipCode === address2.zipCode) {
            return true
        } else {
            return false
        }
}
//ref compare
function areSame(address1, address2) {
    if(address1 == address2) {
        return true
    } else {
        return false
    }
}

console.log("equal? " + areEqual(address1, address2))
console.log("same? " + areSame(address1, address2))