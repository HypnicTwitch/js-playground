
let address = createAddress('a', 'b', 'c')

//Factory function
function createAddress(theStreet, theCity, theZip) {
    return {
        street: theStreet,
        city: theCity,
        zipCode: theZip
    }
}

console.log(address)