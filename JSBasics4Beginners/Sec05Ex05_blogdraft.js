/** create a "blog post" constructor that is for a post
 * that is not yet published
 */
let post = new BlogPost('a', 'b', 'c')

function BlogPost(title, body, author) {
    this.title = title
    this.body = body
    this.author = author
    this.view_count = 0
    this.comments = []
    this.isLive = false
}
console.log(post)